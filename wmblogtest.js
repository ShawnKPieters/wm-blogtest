Blogposts = new Mongo.Collection('blogpost');

if (Meteor.isClient) {
  Meteor.subscribe("blogposts");

  Template.registerHelper('formatDate', function(date) {
    return moment(date).format('MM-DD-YYYY');
  });

  Template.body.helpers({
    blogposts: function(){
      return Blogposts.find();
    }
  });

  Template.body.events({
    'submit .new-blogpost': function(event) {
      var title = event.target.title.value;
      var content = event.target.content.value;

      Meteor.call("addBlogpost", title, content);

      event.target.title.value = "";
      event.target.content.value = "";
      return false;
    }
  });

  Template.blogpost.helpers({
    isOwner: function(){
      return this.owner === Meteor.userId();
    }
  });

  Template.blogpost.events({
    'click .toggle-checked': function() {
      Meteor.call("updateBlogpost", this._id, !this.checked);
    },
    'click .delete': function(){
      Meteor.call("deleteBlogpost", this._id);
    }
  });

  Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });

  Meteor.publish("blogposts", function(){
    return Blogposts.find();
  });
}




Meteor.methods({

    addBlogpost: function(title, content) {
      var date = new Date();
      Blogposts.insert({
        title: title,
        content: content,
        createdAt: date.toDateString(),
        owner: Meteor.userId(),
        username: Meteor.user().username
      });
    },

    updateBlogpost: function(id, checked){
      Blogposts.update(id, {$set: {checked: checked}});
    },

    deleteBlogpost: function(id) {
      Blogposts.remove(id);
    }
});
